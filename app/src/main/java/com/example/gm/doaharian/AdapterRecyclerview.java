package com.example.gm.doaharian;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by gm on 18/03/17.
 */

public class AdapterRecyclerview extends RecyclerView.Adapter<HolderRecyclerview> {

    private Context context;
    private String[] doa;
    private PindahActivity pindahActivity;

    public AdapterRecyclerview(Context context, String[] doa ,PindahActivity pindahActivity) {
        this.context = context;
        this.doa = doa;
        this.pindahActivity = pindahActivity;
    }

    public interface PindahActivity{
        void detail(int position);
    }

    @Override
    public HolderRecyclerview onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_activity,parent,false);
        return new HolderRecyclerview(v);
    }

    @Override
    public void onBindViewHolder(HolderRecyclerview holder, final int position) {
        holder.onBind(doa[position]);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pindahActivity.detail(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return doa.length;
    }
}
