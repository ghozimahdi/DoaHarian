package com.example.gm.doaharian;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle("Do'a Harian");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        AdapterRecyclerview adapter = new AdapterRecyclerview(this, getResources().getStringArray(R.array.doa),
                new AdapterRecyclerview.PindahActivity() {
            @Override
            public void detail(int position) {
                Intent intent = new Intent(HomeActivity.this,MainActivity.class);
                intent.putExtra("position",position);

                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);
    }
}
