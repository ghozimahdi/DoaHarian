package com.example.gm.doaharian;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by gm on 18/03/17.
 */

public class HolderRecyclerview extends RecyclerView.ViewHolder {

    public TextView daftar_judul;
    public ImageView daftar_icon;
    public RelativeLayout container;

    public HolderRecyclerview(View v) {
        super(v);

        daftar_judul = (TextView) v.findViewById(R.id.daftar_judul);
        daftar_icon = (ImageView) v.findViewById(R.id.daftar_icon);
        container = (RelativeLayout) v.findViewById(R.id.container);
    }

    public void onBind(String doa) {
        daftar_judul.setText(doa);
    }
}
