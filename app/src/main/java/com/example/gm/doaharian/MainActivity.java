package com.example.gm.doaharian;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private TextView judul, arab, arti;
    private Button btnplay, btnrestar;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        judul = (TextView) findViewById(R.id.judul);
        arab = (TextView) findViewById(R.id.arab);
        arti = (TextView) findViewById(R.id.arti);

        Intent intent = getIntent();
        final int position = intent.getExtras().getInt("position");

        String[] doa_ = getResources().getStringArray(R.array.doa);
        String[] arti_ = getResources().getStringArray(R.array.artinya);
        String[] arab_ = getResources().getStringArray(R.array.ayat);

        final int[] raw = {R.raw.sblm_tidur, R.raw.setlh_tidur, R.raw.mask_km, R.raw.keluar_km, R.raw.akan_mandi
                , R.raw.berpakaian, R.raw.bercermin, R.raw.sblm_makn, R.raw.sesudah_makn, R.raw.keluar_rumah
                , R.raw.mask_rumah, R.raw.sblm_belajar, R.raw.sesudah_belajar};

        judul.setText(doa_[position]);
        arab.setText(arab_[position]);
        arti.setText(arti_[position]);

        mp = MediaPlayer.create(this, raw[position]);

        btnplay = (Button) findViewById(R.id.btnplay);
        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mp.isPlaying()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        btnplay.setBackground(getResources().getDrawable(R.drawable.pause));
                    }
                    mp.start();
                }
                else if(mp.isPlaying()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        btnplay.setBackground(getResources().getDrawable(R.drawable.play));
                    }
                    mp.pause();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mp.stop();
        mp = null;
    }
}
